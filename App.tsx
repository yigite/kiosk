import React from 'react';
import {StatusBar} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Provider} from 'react-redux';
import store from './src/redux/store';
import { I18nextProvider } from 'react-i18next';
import i18n from './src/config/i18n';
import ErrorBoundary from './src/components/ErrorBoundary';

// Screen imports...
import WelcomeScreen from './src/screens/WelcomeScreen';
import CartScreen from './src/screens/CartScreen';
import SummaryScreen from './src/screens/SummaryScreen';
import SettingsScreen from './src/screens/SettingsScreen';
import PaymentSuccess from './src/screens/PaymentSuccess';
import PaymentFail from './src/screens/PaymentFail';

const Stack = createNativeStackNavigator();

const AppContent = () => {
  return (
    <I18nextProvider i18n={i18n}>
      <NavigationContainer /*theme={DarkTheme}*/>
        <Stack.Navigator
          initialRouteName="Welcome"
          screenOptions={{
            headerShown: false,
            gestureEnabled: true,
            gestureDirection: 'horizontal',
            animation: 'slide_from_right',
          }}>
          <Stack.Screen name="Welcome" component={WelcomeScreen} />
          <Stack.Screen name="Cart" component={CartScreen} />
          <Stack.Screen name="Summary" component={SummaryScreen} />
          <Stack.Screen name="PaymentSuccess" component={PaymentSuccess} />
          <Stack.Screen name="PaymentFail" component={PaymentFail} />
          <Stack.Screen
            name="Settings"
            component={SettingsScreen}
            options={{title: 'Settings'}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </I18nextProvider>
  );
};

const App = () => {
  return (
    <ErrorBoundary>
      <Provider store={store}>
        <StatusBar hidden={true} />
        <AppContent />
      </Provider>
    </ErrorBoundary>
  );
};

export default App;
