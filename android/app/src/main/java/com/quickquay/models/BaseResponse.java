package com.quickquay.models;

import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.Arguments;

public class BaseResponse<T> {

    private boolean isError;
    private String message;
    private T data;

    public BaseResponse(boolean isError, String message, T data) {
        this.isError = isError;
        this.message = message;
        this.data = data;
    }

    public WritableMap toWritableMap() {
        WritableMap map = Arguments.createMap();
        map.putBoolean("isError", isError);
        map.putString("message", message);
        if (data != null) {
            map.putString("data", data.toString());
        }
        return map;
    }
}
