package com.quickquay.modules;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.quickquay.models.BaseResponse;

import android.os.Handler;
import android.os.Looper;
import java.util.Random;

public class PaymentModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;
    private final Handler handler;
    private final Random random;

    public PaymentModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
        this.handler = new Handler(Looper.getMainLooper());
        this.random = new Random();
    }

    @Override
    public String getName() {
        return "PaymentModule";
    }

    @ReactMethod
    public void processPayment(final Callback callback) {
        handler.postDelayed(() -> {
            boolean isSuccess = random.nextBoolean();
            BaseResponse<String> response = new BaseResponse<>(
                    !isSuccess,
                    isSuccess ? "Ödeme başarılı" : "Ödeme başarısız",
                    isSuccess ? "İşlem tamamlandı" : "İşlem başarısız"
            );
            callback.invoke(response.toWritableMap());
        }, 2000);
    }
}
