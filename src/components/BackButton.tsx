import React from 'react';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useTranslation} from 'react-i18next';
import IconBack from '../../assets/images/ic-back.svg';

interface BackButtonProps {
  theme?: 'light' | 'dark';
}

const BackButton: React.FC<BackButtonProps> = (props: BackButtonProps) => {
  const navigation = useNavigation();
  const {t} = useTranslation();

  return (
    <TouchableOpacity
      activeOpacity={0.9}
      style={[
        styles.backButton,
        props.theme === 'dark' && styles.backButtonDark,
      ]}
      onPress={() => navigation.goBack()}>
      <IconBack
        width={12}
        height={24}
        stroke={props.theme === 'dark' ? '#fff' : '#000'}
      />
      <Text
        style={[
          styles.backButtonText,
          props.theme === 'dark' && styles.backButtonTextDark,
        ]}>
        {t('back')}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  backButton: {
    position: 'absolute',
    top: 30,
    left: 20,
    backgroundColor: '#fff',
    borderRadius: 20,
    paddingVertical: 5,
    paddingHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  backButtonDark: {
    backgroundColor: '#000',
  },
  backButtonText: {
    fontSize: 12,
    color: '#000',
    fontFamily: 'NunitoSansMedium',
    paddingBottom: 1,
    paddingEnd: 2,
  },
  backButtonTextDark: {
    color: '#fff',
  },
});

export default BackButton;
