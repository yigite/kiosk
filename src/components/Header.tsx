import React from 'react';
import {StyleSheet, ImageBackground, Animated} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import BackButton from './BackButton';

interface HeaderProps {
  title: string;
  scrollY: Animated.Value;
  HEADER_MAX_HEIGHT: number;
}

const HEADER_MIN_HEIGHT = 100;

const Header: React.FC<HeaderProps> = (props: HeaderProps) => {
  const HEADER_SCROLL_DISTANCE = props.HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

  const headerHeight = props.scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [props.HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
    extrapolate: 'clamp',
  });

  const titleScale = props.scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [1, 0.9, 0.8],
    extrapolate: 'clamp',
  });

  const titlePaddingStart = props.scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [20, 10],
    extrapolate: 'clamp',
  });

  const gradientOverlayHeight = props.scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [150, 75],
    extrapolate: 'clamp',
  });

  console.log('headerHeight:', headerHeight);

  return (
    <Animated.View style={[styles.header, {height: headerHeight}]}>
      <ImageBackground
        source={require('../../assets/images/kiosk-header.jpeg')}
        style={styles.headerImage}>
        <Animated.View
          style={[
            styles.gradientOverlayWrapper,
            {height: gradientOverlayHeight},
          ]}>
          <LinearGradient
            colors={['rgba(255,255,255,0)', 'rgba(255,255,255,1)']}
            style={styles.gradientOverlay}
          />
        </Animated.View>
        <BackButton />
        <Animated.Text
          style={[
            styles.headerTitle,
            {
              transform: [{scale: titleScale}],
              paddingStart: titlePaddingStart,
            },
          ]}>
          {props.title}
        </Animated.Text>
      </ImageBackground>
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#fff',
    overflow: 'hidden',
    zIndex: 1,
  },
  headerImage: {
    width: '100%',
    height: '100%',
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    resizeMode: 'cover',
  },
  gradientOverlayWrapper: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    height: 150,
  },
  gradientOverlay: {
    width: '100%',
    height: '100%',
  },
  headerTitle: {
    fontSize: 30,
    fontFamily: 'PlayfairDisplay-BoldItalic',
    color: '#000',
    marginTop: 50,
    paddingStart: 20,
    paddingBottom: 5,
  },
});

export default Header;
