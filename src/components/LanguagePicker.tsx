import React from 'react';
import {Picker} from '@react-native-picker/picker';
import {StyleSheet} from 'react-native';
import {useTranslation} from 'react-i18next';

const LanguagePicker = React.memo(() => {
  const {i18n} = useTranslation();

  return (
    <Picker
      selectedValue={i18n.language}
      onValueChange={lang => i18n.changeLanguage(lang)}
      style={styles.picker}>
      <Picker.Item label="English" value="en" />
      <Picker.Item label="Türkçe" value="tr" />
    </Picker>
  );
});

const styles = StyleSheet.create({
  picker: {
    height: 50,
    width: 150,
  },
});

export default LanguagePicker;
