import React, {useRef} from 'react';
import {View, StyleSheet, ScrollView, Animated, Image} from 'react-native';
import Header from './Header';
import LinearGradient from 'react-native-linear-gradient';

interface LayoutProps {
  pageTitle: string;
  children: React.ReactNode;
}

const HEADER_MAX_HEIGHT = 250;

const Layout: React.FC<LayoutProps> = (props: LayoutProps) => {
  const scrollY = useRef(new Animated.Value(0)).current;

  return (
    <View style={styles.container}>
      <Header
        title={props.pageTitle}
        scrollY={scrollY}
        HEADER_MAX_HEIGHT={HEADER_MAX_HEIGHT}
      />
      <LinearGradient
        colors={['rgba(255,255,255,1)', 'rgba(255,255,255,0)']}
        style={styles.gradientOverlay}
      />
      <ScrollView
        style={styles.scrollView}
        contentContainerStyle={styles.scrollContent}
        onScroll={Animated.event(
          [{nativeEvent: {contentOffset: {y: scrollY}}}],
          {useNativeDriver: false},
        )}
        scrollEventThrottle={16}>
        {props.children}
      </ScrollView>

      <Image
        source={require('../../assets/images/d2dpx-logo.png')}
        style={[styles.logo, {tintColor: 'black'}]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    flex: 1,
    backgroundColor: '#fff',
  },
  scrollContent: {
    paddingTop: HEADER_MAX_HEIGHT,
  },
  gradientOverlay: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    height: 100,
  },
  logo: {
    position: 'absolute',
    bottom: 5,
    left: 10,
    width: 40,
    height: 40,
    resizeMode: 'contain',
  },
});

export default Layout;
