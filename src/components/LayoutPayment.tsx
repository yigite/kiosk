import React from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
  Text,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

interface LayoutPaymentProps {
  icon: React.ReactNode;
  children: React.ReactNode;
}

const LayoutPayment: React.FC<LayoutPaymentProps> = (
  props: LayoutPaymentProps,
) => {
  const navigation = useNavigation();
  const handleClose = () => {
    navigation.navigate('Welcome' as never);
  };

  return (
    <View style={styles.container}>
      <View style={styles.iconContainer}>{props.icon}</View>
      <ScrollView
        style={styles.scrollView}
        contentContainerStyle={styles.scrollContent}>
        {props.children}
        <TouchableOpacity style={styles.closeButton} onPress={handleClose}>
          <Text style={styles.closeButtonText}>KAPAT</Text>
        </TouchableOpacity>
      </ScrollView>

      <Image
        source={require('../../assets/images/d2dpx-logo.png')}
        style={styles.logo}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#fff',
  },
  iconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  closeButton: {
    backgroundColor: '#000',
    paddingVertical: 15,
    borderRadius: 30,
    alignItems: 'center',
    marginTop: 20,
  },
  closeButtonText: {
    color: '#fff',
    fontSize: 18,
  },
  scrollView: {
    flex: 1,
    backgroundColor: '#fff',
  },
  scrollContent: {},
  gradientOverlay: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    height: 100,
  },
  logo: {
    position: 'absolute',
    bottom: 5,
    left: 10,
    width: 40,
    height: 40,
    resizeMode: 'contain',
    tintColor: 'black',
  },
});

export default LayoutPayment;
