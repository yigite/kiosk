// apiConfig.ts
import AsyncStorage from '@react-native-async-storage/async-storage';
import { ApiConfig } from './types';


export const loadApiConfig = async (): Promise<ApiConfig> => {
    const server = await AsyncStorage.getItem('serverName');
    const user = await AsyncStorage.getItem('username');
    const pass = await AsyncStorage.getItem('password');

    return {
        BASE_URL: server ?? 'http://178.157.14.142:8085/webservices',
        CREDENTIALS: {
            email: user ?? 'admin@dalamanairport.aero',
            password: pass ?? '123456Aa',
        },
    };
};
