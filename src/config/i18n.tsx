import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';

import en from '../locales/en/translation.json';
import tr from '../locales/tr/translation.json';
import ru from '../locales/ru/translation.json';

i18n.use(initReactI18next).init({
  compatibilityJSON: 'v3',
  resources: {
    en: {translation: en},
    tr: {translation: tr},
    ru: {translation: ru},
  },
  fallbackLng: 'en',
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
