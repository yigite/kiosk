// src/config/types.ts
export interface ApiConfig {
  BASE_URL: string;
  CREDENTIALS: {
    email: string;
    password: string;
  };
}
