export const BASE64_IMAGES = {
  DLM_LOGO: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAA...', // Buraya DLM logosunun base64 kodu gelecek
  ERROR_ICON: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAA...', // Buraya hata ikonunun base64 kodu gelecek
};