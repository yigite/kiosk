export const API = {
  BASE_URL: 'http://178.157.14.142:8085/webservices',
  TIMEOUT: 10000,
  CREDENTIALS: {
    username: 'admin@dalamanairport.aero',
    password: '123456Aa',
  },
  ENDPOINTS: {
    AUTH: {
      TOKEN: '/auth/generateDeskToken',
      REFRESH: '/auth/refreshToken',
    },
  },
};

export const ROUTES = {
  WELCOME: 'Welcome',
  CART: 'Cart',
  SETTINGS: 'Settings',
};
