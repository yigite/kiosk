import {SET_AUTH_TOKENS, LOGOUT} from './actionTypes';

interface AuthTokenPayload {
  access_token: string;
  refresh_token: string;
}

export const setAuthTokens = (tokens: AuthTokenPayload) => ({
  type: SET_AUTH_TOKENS,
  payload: tokens,
});

export const logout = () => ({
  type: LOGOUT,
});
