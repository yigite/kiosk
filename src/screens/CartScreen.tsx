import * as React from 'react';
import {useTranslation} from 'react-i18next';
import {
  View,
  Text,
  StyleSheet,
  NativeModules,
  TouchableOpacity,
  Image,
} from 'react-native';
import {fetchAccessToken} from '../services/authService';
import Layout from '../components/Layout';
import {useEffect, useState} from 'react';
const {PaymentModule} = NativeModules;

interface Product {
  id: string;
  name: string;
  quantity: number;
  price: number;
  currency: string;
  currencyCode: string;
}

const CartScreen = ({navigation}: any) => {
  const {t} = useTranslation();

  useEffect(() => {
    const initialize = async () => {
      try {
        const token = await fetchAccessToken();
        if (!token) {
          console.error('No access token obtained');
        }
      } catch (error) {
        console.error('Error fetching access token:', error);
      }
    };
    initialize();
  }, []);

  const [products, setProducts] = useState<Product[]>([
    {
      id: '1',
      name: 'Yetişkin (12 Yaş ve Üzeri)',
      quantity: 1,
      price: 26.0,
      currency: 'EUR',
      currencyCode: '€',
    },
    {
      id: '2',
      name: 'Çocuk 7-12 Yaş Arası',
      quantity: 0,
      price: 14.5,
      currency: 'EUR',
      currencyCode: '€',
    },
  ]);

  const handleQuantityChange = (id: string, delta: number) => {
    setProducts(currentProducts =>
      currentProducts.map(product =>
        product.id === id
          ? {...product, quantity: product.quantity + delta}
          : product,
      ),
    );
  };

  const totalCost = () => {
    return products
      .reduce((acc, product) => acc + product.price * product.quantity, 0)
      .toFixed(2);
  };

  const handlePayment = () => {
    PaymentModule.processPayment((response: any) => {
      if (!response.isError) {
        navigation.navigate('PaymentSuccess');
      } else {
        navigation.navigate('PaymentFail');
      }
    });
  };

  return (
    <Layout pageTitle="Fast Track">
      <View style={styles.contentContainer}>
        <View style={styles.priceSection}>
          <Text style={styles.sectionTitle}>Birim Fiyat</Text>
          {products.map(item => (
            <View key={item.id} style={styles.priceRow}>
              <Text style={styles.priceText}>{item.name}</Text>
              <View style={styles.dashedLine} />
              <Text style={styles.priceValue}>{`${item.price.toFixed(2)} ${
                item.currencyCode
              }`}</Text>
            </View>
          ))}
        </View>

        <View style={styles.passengerSection}>
          <Text style={styles.sectionTitle}>Lütfen Yolcu Adedini Seçiniz</Text>
          {products.map(item => (
            <View key={item.id + 'quantity'} style={styles.passengerRow}>
              <View style={styles.passengerRowBody}>
                <Image
                  source={require('../../assets/images/ic-adult.png')}
                  style={styles.iconAdult}
                />
                <Text style={styles.passengerText}>{item.name}</Text>
              </View>
              <View style={styles.counterContainer}>
                <TouchableOpacity
                  activeOpacity={0.9}
                  onPress={() => handleQuantityChange(item.id, -1)}
                  disabled={item.quantity <= 0}>
                  <Text style={styles.counterButton}>-</Text>
                </TouchableOpacity>
                <Text style={styles.counterText}>{item.quantity}</Text>
                <TouchableOpacity
                  activeOpacity={0.9}
                  onPress={() => handleQuantityChange(item.id, 1)}>
                  <Text style={styles.counterButton}>+</Text>
                </TouchableOpacity>
              </View>
            </View>
          ))}
        </View>

        <View style={styles.totalSection}>
          <Text style={styles.totalText}>{t('total_price')}</Text>
          <Text style={styles.totalValue}>
            {totalCost()} {products[0].currencyCode}
          </Text>
        </View>

        <TouchableOpacity style={styles.purchaseButton} onPress={handlePayment}>
          <Text style={styles.purchaseButtonText}>SATIN AL</Text>
        </TouchableOpacity>
      </View>
    </Layout>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingHorizontal: 5,
    paddingVertical: 5,
  },
  fastTrackText: {
    fontSize: 30,
    fontFamily: 'NunitoSansBold',
    color: '#000',
    marginTop: 50,
  },
  priceSection: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderBottomColor: '#eee',
    borderBottomWidth: 1,
  },
  sectionTitle: {
    fontSize: 18,
    fontFamily: 'NunitoSansBlack',
    marginBottom: 10,
    color: '#000',
  },
  priceRow: {
    padding: 20,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 5,
  },
  dashedLine: {
    flex: 1,
    borderBottomWidth: 1,
    borderStyle: 'dashed',
    borderColor: '#ccc',
    marginHorizontal: 10,
    marginTop: 3,
  },
  priceText: {
    fontFamily: 'NunitoSansMedium',
    fontSize: 16,
    color: '#000',
  },
  priceValue: {
    fontSize: 16,
    fontFamily: 'NunitoSansBold',
    color: '#000',
  },
  passengerSection: {
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  passengerRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  passengerRowBody: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    gap: 10,
  },
  passengerText: {
    fontFamily: 'NunitoSansMedium',
    fontSize: 16,
    color: '#000',
  },
  counterContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: '#000',
    borderRadius: 45,
    paddingVertical: 0,
    paddingHorizontal: 10,
    width: 105,
    gap: 0,
  },
  counterButton: {
    fontSize: 20,
    width: 30,
    fontFamily: 'NunitoSansMedium',
    color: '#fff',
    textAlign: 'center',
    paddingBottom: 4,
  },
  counterText: {
    fontSize: 16,
    fontFamily: 'NunitoSansBold',
    marginHorizontal: 10,
    color: '#fff',
    width: 25,
    textAlign: 'center',
    paddingBottom: 2,
  },
  totalSection: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderTopColor: '#eee',
    borderTopWidth: 1,
  },
  totalText: {
    fontSize: 18,
    fontFamily: 'NunitoSansLight',
  },
  totalValue: {
    fontSize: 28,
    fontFamily: 'NunitoSansExtraBold',
    color: '#000',
  },
  purchaseButton: {
    backgroundColor: '#000',
    paddingVertical: 15,
    marginHorizontal: 20,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  purchaseButtonText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: 'bold',
  },
  logo: {
    position: 'absolute',
    bottom: 5,
    left: 10,
    width: 40,
    height: 40,
    resizeMode: 'contain',
  },
  iconAdult: {
    width: 14,
    height: 35,
    objectFit: 'contain',
  },
  iconChild: {
    width: 22,
    objectFit: 'contain',
  },
});

export default CartScreen;
