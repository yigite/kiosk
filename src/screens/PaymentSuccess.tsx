import React from 'react';
import {View, Text, StyleSheet, Alert, TouchableOpacity} from 'react-native';
import IconCheck from '../../assets/images/ic-check.svg';
import LayoutPayment from '../components/LayoutPayment';
import EscPosPrinter from 'react-native-esc-pos-printer';

const PaymentSuccess = () => {
  const handlePrint = async () => {
    try {
      // Printer'ı başlat
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const printer = EscPosPrinter.init({
        target: 'TCP:192.168.1.100',
        seriesName: 'EPOS2_TM_T20',
        language: 'EPOS2_LANG_EN',
      })
        .then(() => console.log('Init success!'))
        .catch(e => console.log('Init error:', e.message));

      const printing = new EscPosPrinter.printing();

      await printing
        .initialize()
        .align('center')
        .smooth()
        .newline()
        .image(require('../../assets/images/dlm_logo.png'), {
          width: 200,
          halftone: 'EPOS2_HALFTONE_THRESHOLD',
        })
        .newline()
        .textLine(48, {
          left: 'Fasttrack (Adult)',
          right: '2 x  52 EUR',
        })
        .textLine(48, {
          left: 'Fasttrack (Child 0 - 12)',
          right: '3 x  66 EUR',
        })
        .size(1)
        .textLine(48, {
          left: '-',
          right: '-',
          gapSymbol: '-',
        })
        .align('right')
        .size(2)
        .bold()
        .text('Total:  118 EUR')
        .size(1)
        .newline()
        .align('center')
        .textLine(48, {
          left: '-',
          right: '-',
          gapSymbol: '-',
        })
        .line('Fasttarck (Adult) 1')
        .qrcode({
          value: 'Test123',
          width: 5,
          type: 'EPOS2_SYMBOL_QRCODE_MODEL_2',
          level: 'EPOS2_LEVEL_M',
        })
        .textLine(48, {
          left: '-',
          right: '-',
          gapSymbol: '-',
        })
        .line('Fasttarck (Adult) 2')
        .qrcode({
          value: 'Test123',
          width: 8,
          type: 'EPOS2_SYMBOL_QRCODE_MODEL_2',
          level: 'EPOS2_LEVEL_M',
        })
        .cut()
        .send();

    } catch (error) {
      console.error('Yazdırma hatası:', error);
      Alert.alert('Hata', 'Yazdırma işlemi başarısız oldu.');
    } finally {
      console.log('Yazdırma işlemi tamamlandı');
    }
  };

  return (
    <LayoutPayment icon={<IconCheck width={96} height={96} />}>
      <View>
        <Text style={styles.title}>Ödeme Başarılı</Text>
        <Text style={styles.subTitle}>Sipariş Özeti</Text>

        {/* Sipariş Özeti */}
        <View style={styles.summary}>
          <View style={styles.row}>
            <Text style={styles.label}>Yetişkin (12 Yaş+)</Text>
            <View style={styles.dashedLine} />
            <Text style={styles.price}>26,00 € +KDV</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.label}>Çocuk (7-12 Yaş)</Text>
            <View style={styles.dashedLine} />
            <Text style={styles.price}>14,50 € +KDV</Text>
          </View>
        </View>

        {/* Toplam Ücret */}
        <View style={styles.total}>
          <Text style={styles.totalLabel}>Ödenen Toplam Ücret</Text>
          <Text style={styles.totalPrice}>76,00 € (KDV Dahil)</Text>
        </View>

        <TouchableOpacity 
          onPress={handlePrint}
          style={styles.printButton}
        >
          <Text style={styles.printButtonText}>Fiş Yazdır</Text>
        </TouchableOpacity>
      </View>
    </LayoutPayment>
  );
};

const styles = StyleSheet.create({
  backButton: {
    backgroundColor: '#000',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 30,
    alignSelf: 'flex-start',
  },
  backButtonText: {
    color: '#fff',
    fontSize: 16,
  },
  iconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  title: {
    fontSize: 24,
    fontFamily: 'NunitoSansBold',
    textAlign: 'center',
    marginVertical: 10,
    color: '#000',
  },
  subTitle: {
    fontSize: 24,
    fontFamily: 'NunitoSansLight',
    textAlign: 'center',
    marginBottom: 20,
    color: '#000',
  },
  summary: {
    paddingTop: 12,
    paddingBottom: 24,
    borderTopColor: '#eee',
    borderTopWidth: 1,
    borderBottomColor: '#eee',
    borderBottomWidth: 1,
    marginBottom: 20,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 12,
  },
  label: {
    fontSize: 16,
    fontFamily: 'NunitoSansMedium',
    color: '#000',
  },
  dashedLine: {
    flex: 1,
    borderBottomWidth: 1,
    borderStyle: 'dashed',
    borderColor: '#eee',
    marginHorizontal: 10,
  },
  price: {
    fontSize: 16,
    fontFamily: 'NunitoSansBold',
    textAlign: 'right',
    color: '#000',
  },
  total: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingBottom: 24,
    borderBottomWidth: 1,
    borderColor: '#eee',
    marginBottom: 10,
  },
  totalLabel: {
    fontSize: 18,
    fontFamily: 'NunitoSansLight',
    color: '#000',
  },
  totalPrice: {
    fontSize: 28,
    fontFamily: 'NunitoSansExtraBold',
    color: '#000',
  },
  closeButton: {
    backgroundColor: '#000',
    paddingVertical: 15,
    borderRadius: 30,
    alignItems: 'center',
    marginTop: 20,
  },
  closeButtonText: {
    color: '#fff',
    fontSize: 18,
  },
  printButton: {
    backgroundColor: '#000',
    paddingVertical: 15,
    borderRadius: 30,
    alignItems: 'center',
    marginTop: 20,
  },
  printButtonText: {
    color: '#fff',
    fontSize: 18,
  },
});

export default PaymentSuccess;
