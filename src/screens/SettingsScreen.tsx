// Bu sayfa tasarıma dahil değil..
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  Button,
  StyleSheet,
  Alert,
  BackHandler,
} from 'react-native';
import {fetchAccessToken} from '../services/authService'; // Ensure path is correct
import AsyncStorage from '@react-native-async-storage/async-storage';
import DeviceInfo from 'react-native-device-info';
import BackButton from '../components/BackButton';
import LanguagePicker from '../components/LanguagePicker';

interface SettingsProps {
  serverName: string;
  username: string;
  password: string;
  printerIp: string;
  printerPort: string;
}

const SettingsScreen: React.FC<{navigation: any}> = ({navigation}) => {
  const [settings, setSettings] = useState<SettingsProps>({
    serverName: '',
    username: '',
    password: '',
    printerIp: '192.168.1.1',
    printerPort: '9100',
  });
  const [loginStatus, setLoginStatus] = useState<boolean>(false);
  const [deviceUID, setDeviceUID] = useState<string>('');

  useEffect(() => {
    loadInitialSettings();
    generateDeviceUID();
  }, []);
  const generateDeviceUID = async () => {
    // Assuming a UID generation logic or retrieving a stored UID
    const uid =
      (await AsyncStorage.getItem('deviceUID')) ||
      (await DeviceInfo.getDeviceId());
    setDeviceUID(uid);
    await AsyncStorage.setItem('deviceUID', uid); // Store it if not already stored
  };
  const loadInitialSettings = async () => {
    const token = await AsyncStorage.getItem('accessToken');
    const server = await AsyncStorage.getItem('serverName').catch(error => {
      console.error('AsyncStorage error: ', error.message);
    });
    const user = await AsyncStorage.getItem('username');
    const pass = await AsyncStorage.getItem('password');
    const printerIp = await AsyncStorage.getItem('printerIp');
    const printerPort = await AsyncStorage.getItem('printerPort');
    setSettings({
      serverName: server ?? '',
      username: user ?? '',
      password: pass ?? '',
      printerIp: printerIp ?? '192.168.1.1',
      printerPort: printerPort ?? '9100',
    });
    setLoginStatus(!!token);
  };

  const handleSave = async () => {
    await AsyncStorage.multiSet([
      ['serverName', settings.serverName],
      ['username', settings.username],
      ['password', settings.password],
    ]).catch(error => {
      console.error('AsyncStorage error: ', error.message);
    });

    Alert.alert('Settings Saved!', 'Attempting to log in...', [
      {text: 'OK', onPress: () => handleLogin()},
    ]);
  };

  const handleLogin = async () => {
    try {
      const response = await fetchAccessToken();
      if (response.access_token) {
        await AsyncStorage.setItem('accessToken', response.access_token);
        setLoginStatus(true);
        Alert.alert('Login successful!', 'Redirecting to welcome screen.', [
          {text: 'OK', onPress: () => navigation.navigate('Welcome')}, // Redirect to Welcome screen
        ]);
      }
    } catch (error) {
      await handleLogout();
      console.error('Login failed:', error);
      Alert.alert('Login failed', 'Failed to log in with provided credentials');
    }
  };

  const handleLogout = async () => {
    await AsyncStorage.removeItem('accessToken');
    await AsyncStorage.removeItem('refreshToken');
    setLoginStatus(false);
    Alert.alert('Logged out successfully');
  };

  const handleCloseApp = () => {
    BackHandler.exitApp(); // Only works on Android
  };

  return (
    <View style={styles.container}>
      <BackButton />
      <LanguagePicker />
      <TextInput
        placeholder="Server Name"
        value={settings.serverName}
        onChangeText={text => setSettings({...settings, serverName: text})}
        style={styles.input}
      />
      <TextInput
        placeholder="Username"
        value={settings.username}
        onChangeText={text => setSettings({...settings, username: text})}
        style={styles.input}
      />
      <TextInput
        placeholder="Password"
        value={settings.password}
        secureTextEntry
        onChangeText={text => setSettings({...settings, password: text})}
        style={styles.input}
      />
      <TextInput editable={false} value={deviceUID} style={styles.input} />

      <Text style={styles.sectionTitle}>Yazıcı Ayarları</Text>
      <TextInput
        placeholder="Yazıcı IP Adresi"
        value={settings.printerIp}
        onChangeText={text => setSettings({...settings, printerIp: text})}
        style={styles.input}
      />
      <TextInput
        placeholder="Yazıcı Port"
        value={settings.printerPort}
        onChangeText={text => setSettings({...settings, printerPort: text})}
        style={styles.input}
        keyboardType="numeric"
      />

      <Text style={styles.statusText}>
        {loginStatus ? 'You are logged in' : 'You are logged out'}
      </Text>
      <View style={styles.buttonContainer}>
        <Button title="Save Settings" onPress={handleSave} color="#007AFF" />
        <View style={styles.space} />
        <Button
          title="Login"
          onPress={handleLogin}
          disabled={loginStatus}
          color="#007AFF"
        />
        <View style={styles.space} />
        <Button
          title="Logout"
          onPress={handleLogout}
          disabled={!loginStatus}
          color="#FF3B30"
        />
        <View style={styles.space} />
        <Button title="Close App" onPress={handleCloseApp} color="#007AFF" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  backButton: {
    marginBottom: 20,
    alignSelf: 'flex-start',
  },
  input: {
    width: '100%',
    marginVertical: 8,
    borderWidth: 1,
    borderColor: 'gray',
    padding: 10,
    color: '#333333', // Dark grey color for text
  },
  statusText: {
    fontSize: 16,
    marginVertical: 10,
    color: '#333333',
  },
  buttonContainer: {
    marginTop: 20,
    width: '100%',
  },
  space: {
    height: 10,
  },
  sectionTitle: {
    fontSize: 18,
    marginBottom: 10,
    color: '#333333',
  },
});

export default SettingsScreen;
