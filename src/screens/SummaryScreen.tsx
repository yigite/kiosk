import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import {apiService} from '../services/apiService';
import BackButton from '../components/BackButton';
import LanguagePicker from '../components/LanguagePicker';
import {GenerateQrResponse} from '../types';

const SummaryScreen = ({route}: any) => {
  const {products} = route.params;
  const [qrResponses, setQrResponses] = useState<GenerateQrResponse[]>([]);

  useEffect(() => {
    const fetchQrCodes = async () => {
      let allResponses: GenerateQrResponse[] = [];
      for (const product of products) {
        for (let i = 0; i < product.quantity; i++) {
          const qrData = {
            userUID: '0',
            orderCode: product.id,
            boardingType: '200101',
            remainingUsageCount: 3,
            locationId: '7',
          };
          const response = await apiService.generateQrCode(qrData);
          allResponses.push(response);
        }
      }
      setQrResponses(allResponses);
    };

    fetchQrCodes();
  }, [products]);

  return (
    <View style={styles.container}>
      <BackButton theme="dark" />
      <LanguagePicker />
      <Text style={styles.header}>Sipariş Özeti</Text>
      <ScrollView>
        {qrResponses.map((response, index) => (
          <View key={index} style={styles.qrDetailsContainer}>
            <Text style={styles.qrDetail}>User ID: {response.userId}</Text>
            <Text style={styles.qrDetail}>Code: {response.code}</Text>
            <Text style={styles.qrDetail}>Type: {response.type}</Text>
            <Text style={styles.qrDetail}>Message: {response.message}</Text>
            <View style={styles.qrContainer}>
              <QRCode
                value={response.code}
                size={200}
                color="black"
                backgroundColor="white"
              />
            </View>
          </View>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    alignItems: 'center',
  },
  header: {
    fontSize: 22,
    marginBottom: 20,
  },
  qrDetailsContainer: {
    marginTop: 20,
    alignItems: 'center',
    marginBottom: 20,
  },
  qrDetail: {
    fontSize: 16,
    marginVertical: 5,
  },
  qrContainer: {
    marginTop: 10,
    padding: 10,
    borderWidth: 5,
    borderColor: 'gray',
    backgroundColor: 'white',
  },
});

export default SummaryScreen;
