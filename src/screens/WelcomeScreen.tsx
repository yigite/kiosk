/* eslint-disable @typescript-eslint/no-unused-vars */
import React, {useState, useEffect, useCallback} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  ImageBackground,
  GestureResponderEvent,
  Dimensions,
  Button,
} from 'react-native';
import {fetchAccessToken} from '../services/authService';

import {useTranslation} from 'react-i18next';
import LinearGradient from 'react-native-linear-gradient';
import FlagTr from '../../assets/images/flag-tr.svg';
import FlagEn from '../../assets/images/flag-en.svg';

const {width} = Dimensions.get('window');

interface WelcomeScreenProps {
  navigation: any;
}

const WelcomeScreen: React.FC<WelcomeScreenProps> = ({navigation}) => {
  const {t, i18n} = useTranslation();
  const [isLoading, setIsLoading] = useState(false);
  const [loginError, setLoginError] = useState<string | null>(null);

  // Gizli pattern için state'ler
  const [rightTopTapCount, setRightTopTapCount] = useState(0);
  const [leftTopLongPressTimer, setLeftTopLongPressTimer] =
    useState<NodeJS.Timeout | null>(null);
  const [lastTapTime, setLastTapTime] = useState(0);

  useEffect(() => {
    const initializeApp = async () => {
      try {
        setIsLoading(true);
        setLoginError(null);
        const token = await fetchAccessToken();
        if (token) {
          console.log('Token başarıyla alındı');
        }
      } catch (error) {
        console.error('Token alınamadı:', error);
        // Kullanıcı dostu hata mesajı
        setLoginError(
          error instanceof Error
            ? 'Giriş yapılamadı. Lütfen daha sonra tekrar deneyin.'
            : 'Bilinmeyen bir hata oluştu',
        );
      } finally {
        setIsLoading(false);
      }
    };

    initializeApp();
  }, []);

  const handleToCart = async () => {
    setIsLoading(true);
    try {
      await navigation.navigate('Cart');
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  const handleToSettings = async () => {
    await navigation.navigate('Settings');
  };

  const changeLanguage = useCallback(
    (lang: string) => {
      if (i18n.language !== lang) {
        i18n.changeLanguage(lang);
      }
    },
    [i18n],
  );

  // Sağ üst köşe dokunma kontrolü
  const handleRightTopTap = (event: GestureResponderEvent) => {
    const {locationX, locationY} = event.nativeEvent;
    const currentTime = Date.now();

    if (locationX > width - 100 && locationY < 100) {
      // Sağ üst köşe kontrolü
      if (currentTime - lastTapTime < 1000) {
        // 1 saniye içinde
        setRightTopTapCount(prev => prev + 1);
        if (rightTopTapCount === 4) {
          // 5. dokunuşta
          setRightTopTapCount(0);
          handleToSettings();
        }
      } else {
        setRightTopTapCount(1);
      }
      setLastTapTime(currentTime);
    }
  };

  // Sol üst köşe uzun basma kontrolü
  const handleLeftTopPressIn = (event: GestureResponderEvent) => {
    const {locationX, locationY} = event.nativeEvent;

    if (locationX < 100 && locationY < 100) {
      // Sol üst köşe kontrolü
      const timer = setTimeout(() => {
        handleToSettings();
      }, 2000); // 2 saniye basılı tutma

      setLeftTopLongPressTimer(timer);
    }
  };

  const handleLeftTopPressOut = () => {
    if (leftTopLongPressTimer) {
      clearTimeout(leftTopLongPressTimer);
      setLeftTopLongPressTimer(null);
    }
  };

  return (
    <View
      style={styles.container}
      onTouchStart={handleRightTopTap}
      onTouchEnd={handleLeftTopPressOut}>
      <ImageBackground
        source={require('../../assets/images/airport-background.jpeg')}
        style={styles.backgroundImage}>
        <LinearGradient
          colors={[
            'rgba(0,0,0,95)',
            'rgba(0,0,0,0.93)',
            'rgba(0,0,0,0.91)',
            'rgba(0,0,0,0.89)',
            'rgba(0,0,0,0.87)',
            'rgba(0,0,0,0.70)',
            'rgba(0,0,0,0.02)',
            'rgba(0,0,0,0.01)',
            'rgba(0,0,0,0)',
          ]}
          style={styles.overlay}
        />
        <View style={styles.middleSection}>
          <View style={styles.languageContainer}>
            <TouchableOpacity
              style={styles.languageButton}
              activeOpacity={1}
              onPress={() => changeLanguage('tr')}>
              <FlagTr width={20} height={20} />
              <Text style={styles.languageText}>TÜRKÇE</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.languageButton}
              activeOpacity={1}
              onPress={() => changeLanguage('en')}>
              <FlagEn width={20} height={20} />
              <Text style={styles.languageText}>ENGLISH</Text>
            </TouchableOpacity>
          </View>

          <TouchableOpacity
            style={styles.purchaseButton}
            activeOpacity={0.9}
            onPress={handleToCart}>
            <Text
              // eslint-disable-next-line react-native/no-inline-styles
              style={[styles.purchaseButtonText, {fontFamily: 'NunitoSans'}]}>
              {t('fastTrack')}
            </Text>
          </TouchableOpacity>
        </View>
        <Image
          source={require('../../assets/images/d2dpx-logo.png')}
          style={styles.logo}
        />
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    fontFamily: 'Nunito Sans',
  },
  overlay: {
    ...StyleSheet.absoluteFillObject,
  },
  languageContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  languageButton: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 10,
    gap: 5,
  },
  flagIcon: {
    width: 20,
    height: 20,
    marginRight: 5,
  },
  languageText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
    fontFamily: 'NunitoSansRegular',
  },
  middleSection: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  purchaseButton: {
    backgroundColor: '#fff',
    paddingVertical: 15,
    paddingHorizontal: 50,
    borderRadius: 25,
    marginTop: 30,
  },
  purchaseButtonText: {
    color: '#000',
    fontWeight: 'bold',
    fontSize: 16,
    fontFamily: 'NunitoSansRegular',
  },
  backgroundImage: {
    flex: 1,
    width: '100%',
    justifyContent: 'flex-end',
    resizeMode: 'cover',
  },
  logo: {
    position: 'absolute',
    bottom: 5,
    left: 20,
    width: 60,
    height: 60,
    resizeMode: 'contain',
  },
});

export default WelcomeScreen;
