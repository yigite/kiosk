// apiService.ts
import apiInstance from '../config/axiosConfig';
import {
  BaseResponse,
  GenerateQrRequest,
  GenerateQrResponse,
  Product,
} from '../types';

class ApiService {
  async generateQrCode(data: GenerateQrRequest): Promise<GenerateQrResponse> {
    const response = await apiInstance.post<GenerateQrResponse>(
      '/djpstore/qrController/generateQrForFiori',
      data,
    );
    return response.data;
  }

  async getProductList(): Promise<Product[]> {
    const response = await apiInstance.get<BaseResponse<Product[]>>(
      '/productController/getProductList',
      {
        params: {
          pageType: 'ALL',
        },
      },
    );

    if (response.data.isError) {
      throw new Error(response.data.message);
    }

    return response.data.value;
  }
}

export const apiService = new ApiService();
