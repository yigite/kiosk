import {httpClient} from './httpClient';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {API} from '../constants';
import {TokenResponse, User} from '../types/auth';
import {BaseResponse} from '../types/api';
import {decodeToken} from '../utils/jwt';
import apiInstance from '../config/axiosConfig';

let currentUser: User | null = null;

export const getCurrentUser = () => currentUser;

export const fetchAccessToken = async () => {
  try {
    const savedCredentials = await AsyncStorage.multiGet([
      'username',
      'password',
    ]);
    const credentials = {
      email: savedCredentials[0][1] || API.CREDENTIALS.username,
      password: savedCredentials[1][1] || API.CREDENTIALS.password,
    };

    const response = await httpClient.post<BaseResponse<TokenResponse>>(
      API.ENDPOINTS.AUTH.TOKEN,
      credentials,
    );

    if (response.data && !response.data.isError) {
      const tokenData = response.data.value;
      if (tokenData?.access_token) {
        await AsyncStorage.multiSet([
          ['accessToken', tokenData.access_token],
          ['refreshToken', tokenData.refresh_token],
          ['username', credentials.email],
          ['password', credentials.password],
        ]);

        try {
          currentUser = decodeToken(tokenData.access_token);
          console.log('Decoded user:', currentUser);
        } catch (decodeError) {
          console.warn('Token decode error:', decodeError);
        }
        return tokenData; // Return the entire token response instead of just access_token
      }
    }

    throw new Error(response.data.message || 'Invalid token response');
  } catch (error) {
    console.error('Token Error:', error);
    throw error;
  }
};

export const refreshAccessToken = async (
  refreshToken: string,
): Promise<TokenResponse> => {
  try {
    const response = await httpClient.post<BaseResponse<TokenResponse>>(
      '/auth/refreshToken',
      {
        refreshToken,
      },
    );

    if (response.data.isError) {
      throw new Error(response.data.message);
    }

    const tokenData = response.data.value;
    await AsyncStorage.multiSet([
      ['accessToken', tokenData.access_token],
      ['refreshToken', tokenData.refresh_token],
    ]);
    currentUser = decodeToken(tokenData.access_token);

    return tokenData;
  } catch (error) {
    console.error('Token yenileme hatası:', error);
    return fetchAccessToken(); // Yenileme başarısız olursa yeni token al
  }
};

apiInstance.interceptors.response.use(
  response => response,
  async error => {
    const originalRequest = error.config;
    if (
      (error.response?.status === 401 || error.response?.status === 403) &&
      !originalRequest._retry
    ) {
      originalRequest._retry = true;
      const refreshToken = await AsyncStorage.getItem('refreshToken');
      console.error('Token refresh needed:', refreshToken);
      try {
        if (refreshToken) {
          const newTokens = await refreshAccessToken(refreshToken);
          apiInstance.defaults.headers.common.Authorization = `${newTokens.token_type} ${newTokens.access_token}`;
          originalRequest.headers.Authorization = `${newTokens.token_type} ${newTokens.access_token}`;
          return apiInstance(originalRequest);
        }
      } catch (refreshError) {
        console.error('Token refresh or initial fetch failed:', refreshError);
        throw refreshError;
      }
    }
    return Promise.reject(error);
  },
);

export default apiInstance;
