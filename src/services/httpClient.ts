import axios, {AxiosInstance, AxiosRequestConfig} from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {API} from '../constants';

export class HttpClient {
  private _httpClient: AxiosInstance;

  constructor() {
    this._httpClient = axios.create({
      baseURL: API.BASE_URL,
      timeout: API.TIMEOUT,
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    });

    this._httpClient.interceptors.request.use(
      async config => {
        const accessToken = await AsyncStorage.getItem('accessToken');
        if (accessToken) {
          config.headers.Authorization = `Bearer ${accessToken}`;
        }
        return config;
      },
      error => Promise.reject(error),
    );

    this._httpClient.interceptors.response.use(
      response => response,
      async error => {
        const originalRequest = error.config;
        if (
          (error.response?.status === 401 || error.response?.status === 403) &&
          !originalRequest._retry
        ) {
          originalRequest._retry = true;
          try {
            const refreshToken = await AsyncStorage.getItem('refreshToken');
            if (refreshToken) {
              const newTokens = await this.refreshToken(refreshToken);
              await this.setTokens(newTokens);
              originalRequest.headers.Authorization = `Bearer ${newTokens.access_token}`;
              return this._httpClient(originalRequest);
            }
          } catch (refreshError) {
            await this.clearTokens();
            throw refreshError;
          }
        }
        return Promise.reject(error);
      },
    );
  }

  private async refreshToken(refreshToken: string) {
    const response = await this._httpClient.post(API.ENDPOINTS.AUTH.REFRESH, {
      refreshToken,
    });
    return response.data.value;
  }

  private async setTokens(tokens: any) {
    await AsyncStorage.multiSet([
      ['accessToken', tokens.access_token],
      ['refreshToken', tokens.refresh_token],
    ]);
  }

  private async clearTokens() {
    await AsyncStorage.multiRemove(['accessToken', 'refreshToken']);
  }

  async get<T>(url: string, config?: AxiosRequestConfig) {
    return this._httpClient.get<T>(url, config);
  }

  async post<T>(url: string, data?: any, config?: AxiosRequestConfig) {
    return this._httpClient.post<T>(url, data, config);
  }
}

export const httpClient = new HttpClient();
