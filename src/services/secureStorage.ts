import EncryptedStorage from 'react-native-encrypted-storage';

export const setSecureItem = async (key: string, value: string) => {
  await EncryptedStorage.setItem(key, value);
};

export const getSecureItem = async (key: string) => {
  return await EncryptedStorage.getItem(key);
};
