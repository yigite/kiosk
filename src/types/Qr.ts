export interface GenerateQrRequest {
  userUID: string;
  orderCode: string;
  boardingType: string;
  remainingUsageCount: number;
  locationId: string;
}

export interface GenerateQrResponse {
  code: string;
  userId: string;
  type: string;
  message: string;
}
