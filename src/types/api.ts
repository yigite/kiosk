export interface BaseResponse<T> {
  isError: boolean;
  message: string;
  status: string;
  value: T;
}
