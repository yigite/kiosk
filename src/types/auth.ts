export interface TokenResponse {
  access_token: string;
  token_type: string;
  scope: string;
  refresh_token: string;
  expires_in: number;
}

export interface User {
  uid: string;
  name: string;
  userType: string;
}
