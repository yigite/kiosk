export interface Direction {
  ordinal: number;
  name: string;
  localizedName: string;
}

export interface FlightType {
  ordinal: number;
  name: string;
  localizedName: string;
}

export interface AgeGroup {
  ordinal: number;
  name: string;
  localizedName: string;
}

export interface ServiceName {
  ordinal: number;
  name: string;
  localizedName: string;
}

export interface GateNumber {
  id: string;
  name: string;
  gateNumber: string;
  serviceName: ServiceName;
  flightType: FlightType;
}

export interface GateWsDto {
  gateNumberResponseData: GateNumber[];
}

export interface Product {
  productId: string;
  productName: string;
  productCode: string;
  direction: Direction;
  flightType: FlightType;
  ageGroup: AgeGroup;
  serviceName: ServiceName;
  productAliasName: string;
  note: string;
  gateWsDto: GateWsDto;
  tax: string;
  forDesk: boolean;
  forWeb: boolean;
  forKiosk: boolean;
  forAgent: boolean;
}
