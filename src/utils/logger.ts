export const logger = {
  error: (message: string, error?: any) => {
    if (__DEV__) {
      console.error(message, error);
    }
    // Production ortamında error tracking servisi entegrasyonu
  }
};
